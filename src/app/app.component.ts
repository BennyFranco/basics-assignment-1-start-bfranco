import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name = '';
  sendName = '';

  itsOk = false;
  itsNotOk = false;

  private time = 2000;

  checkIfOk() {
    if (this.name !== '') {
      this.sendName = this.name;
      this.itsOk = true;
      this.itsNotOk = false;
    } else {
      this.itsNotOk = true;
      this.itsOk = false;
    }

    setTimeout(() => {
      this.itsOk = false;
      this.itsNotOk = false;
    }, this.time);
  }
}
